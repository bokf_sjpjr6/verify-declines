SELECT c.CustomerNumber,w.PropertyId,ef.*,es.ShrinkGas
FROM BDW.Energy.EconomicForecast ef
JOIN BDW.Energy.Customer c on c.CustomerID=ef.CustomerID
JOIN BDW.Energy.Well w on w.WellID=ef.WellID
JOIN BDW.Energy.EconomicSummary es on es.BOKFID=ef.BOKFID
WHERE ef.BOKFID IN ('BOKFID_DUMMY')
;
