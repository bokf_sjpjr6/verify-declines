# Plots Declines from:
# 1) Capital Efficiency - "..._CE"
#     Capital Efficiency output (DROI Export.csv) with 7 columns added manually:
#     qi.o_SS,Di.o_SS,b.o_SS,qi.g_SS,Di.g_SS,b.g_SS,DateStart_SS
# 2) Monthly Data from BDW - "..._MO"
# 3) Source System - "..._SS"

# Checks on scraping Source System Data
## Get Daily Rates from Aries. Get Monthly Rates from PHDWin2
## Check units are correct (not scf)
## Check using correct (usually longest) segment for PHDWin2 if multiple segments

## Shrink in the original example for PHD was 1-Shirnk, but that has been fixed in Data
### And it has bee fixed below

#Packages & Functions
library("pacman")
pacman::p_load(dplyr,readxl,odbc,stringr,tidyr,mefa4,lubridate,aRpsDCA,tibble,ggplot2)
source('Convert Declines.R')

#-------------------------------------------------------------------
####                  Get Decline Data                          ####
#-------------------------------------------------------------------

# Import Decline Curve Data (DROI Export + "..._SS" Columns)
df_declines_RAW <- read.csv("~/GIT REPO-Verify Type Curves/DROI Export - WITH SS Declines.csv")
df_declines<-df_declines_RAW%>%
  as.data.frame()%>%
  # Change CustomerNumber from Integer to Date
  mutate(CustNum=as.character(CustNum))%>%
  # Rename CE decline variables with "_CE" designator to distinguish from "_SS"
  rename(qi.o_CE=qi.o,Di.o_CE=Di.o,b.o_CE=b.o,qi.g_CE=qi.g,Di.g_CE=Di.g,b.g_CE=b.g)%>%
  # FirstProductionDate isn't used. Remove to avoid confusion.
  ## (first_prod, hyp_fitter_month1, DateStart_SS are used)
  ## I think FirstProductionDate needs as.Date wrapper on export otherwise it's an unusable number in CSV
  select(-FirstProductionDate)%>%
  # Fix Price Deck when it comes in as 4 digits
  mutate(PriceDeck=case_when(nchar(PriceDeck)==4 
                             ~ paste0('0',as.character(PriceDeck))
                             ,TRUE ~ as.character(PriceDeck)))%>%
  # Rename dates to start with "DateStart" to match DateStart_SS
  rename(DateStart_CE=first_prod,DateStartHyp_CE=hyp_fitter_month1)%>%
  # Covert CSV Date integer to Date Type
  mutate(DateStart_CE=as.Date(DateStart_CE,"%m/%d/%Y"),DateStart_SS=as.Date(DateStart_SS,"%m/%d/%Y"),
         DateStartHyp_CE=as.Date(DateStartHyp_CE,"%m/%d/%Y"))%>%
  # Filter out where I haven't gotten Source System (SS) data
  filter(is.na(DateStart_SS)==FALSE)%>%
  # Select Columns needed for plotting/analysis
  select(CustName,CustNum,propnum,PriceDeck,DBNum,SourceSystem,
         qi.o_SS,Di.o_SS,b.o_SS,qi.g_SS,Di.g_SS,b.g_SS,DateStart_SS,
         qi.o_CE,Di.o_CE,b.o_CE,qi.g_CE,Di.g_CE,b.g_CE,
         DateStart_CE,DateStartHyp_CE,DateStart_SS,
         prod_profile,INITIAL_STAGE_GAS,LeaseName,reservoir)%>%
  # Get unique records (probably unnecessary)
  unique()

#-------------------------------------------------------------------
####                       Get Monthly                          ####
#-------------------------------------------------------------------

# Get All BOKFID's as string to Query for monthly
BOKFID.all<-paste(df_declines$propnum,df_declines$CustNum,
      df_declines$DBNum,df_declines$PriceDeck,
      sep = '_',collapse = "','")

# Query for monthly
source("Parse SQL Script.R")
sql_statement.raw<-getSQL("Get Monthly.sql")
sql_statement.final<-str_replace(sql_statement.raw,'BOKFID_DUMMY',BOKFID.all)
db <- DBI::dbConnect(odbc::odbc(), "ODBC7")
monthly.all <- DBI::dbGetQuery(db,statement = sql_statement.final)

#-------------------------------------------------------------------------------
####      Get Adjustment Factors From Monthly (NRI and Shrink)              ####
#-------------------------------------------------------------------------------
adjustments_factors<-monthly.all%>%
  group_by(CustomerNumber,PropertyId)%>%
  summarize(NRI=first(NetRevenueInterest,order_by = ForecastMonth),
            Shrink=first(ShrinkGas,order_by = ForecastMonth),
            .groups='drop')

#-------------------------------------------------------------------------------
####                 Adjust Decline Parameters                              ####
#-------------------------------------------------------------------------------

df.all<-df_declines%>%
  as_tibble()%>%
  # Join Adjustment factors onto decline data
  left_join(adjustments_factors,by=c('CustNum'='CustomerNumber','propnum'='PropertyId'))%>%
  # For Aries, Multiply Daily rates by 30. (PHD already in Monthly)
  ## This gets Aries SS' Q to Monthly Rate to match CE/Monthly
  mutate(qi.o_SS=case_when(SourceSystem=='ARIES'
                           ~ 30*qi.o_SS,
                           TRUE ~ as.numeric(qi.o_SS)))%>%
  mutate(qi.g_SS=case_when(SourceSystem=='ARIES'
                           ~ 30*qi.g_SS,
                           TRUE ~ as.numeric(qi.g_SS)))%>%
  # Multiply Source System Gas by Shrink Factor to get it to match CE/Monthly
  mutate(qi.g_SS=case_when(SourceSystem=='ARIES'
                           ~ qi.g_SS*Shrink, # Aries
                           # TRUE ~ qi.g_SS*(1-Shrink)))%>% # PHD
                           TRUE ~ qi.g_SS*(Shrink)))%>% # PHD
  
  # mutate(qi.g_SS=qi.g_SS*(Shrink))%>% # Aries
  # Multiply Source System Q's by NRI bc it is Gross originally and CE/Monthly are Net
  mutate(qi.o_SS=qi.o_SS*NRI)%>%
  mutate(qi.g_SS=qi.g_SS*NRI)%>%
  # Convert Aries (Secant Method) to PHD (Tangent Method). (fx Requires input as percentage)
  mutate(Di.o_SS=case_when(SourceSystem=='ARIES'
                               ~ Decline.Aries_to_PHD(Di.o_SS,b.o_SS),
                               TRUE ~ Di.o_SS))%>%
  mutate(Di.g_SS=case_when(SourceSystem=='ARIES'
                               ~ Decline.Aries_to_PHD(Di.g_SS,b.g_SS),
                               TRUE ~ Di.g_SS))%>%
  # Convert Percentage to Decimal
  mutate(Di.o_SS=Di.o_SS/100)%>%
  mutate(Di.g_SS=Di.g_SS/100)%>%
  # Convert Effective to Nominal Decline ("arps.decline" fx requires input as nominal)
  mutate(Di.o_SS=as.nominal(Di.o_SS,from.period = 'year',to.period = 'month'))%>%
  mutate(Di.g_SS=as.nominal(Di.g_SS,from.period = 'year',to.period = 'month'))

#-------------------------------------------------------------------
####                       Plot Data                            ####
#-------------------------------------------------------------------

# Filter which wells to plot
df_to_plot<-df.all%>%
  # filter(CustName %in% c('SILVER HILL ENERGY PARTNERS III LLC'))%>%
  filter(CustName %in% c('MOSS CREEK RESOURCES LLC','UNIT CORPORATION AND SUBSIDIARIES','PILLAR HOLDINGS LLC'))%>%
  # filter(CustName %in% c('RANGE RESOURCES CORPORATION'))%>%
  # filter(CustName %in% c('VIPER ENERGY PARTNERS LLC'))%>%
  # filter(LeaseName %in% c('CLARICE STARLING JESSUP A 1421LS','ALLRED UNIT B 08-05 6MH','ALLRED UNIT A 08-05 4CH','FRYAR UNIT A 13-12 2SH'))%>%
  # filter(grepl('SAFI', LeaseName)==TRUE)%>%
  # filter(LeaseName %in% c('CLARICE STARLING JESSUP A 1421LS'))%>%
  # filter(SourceSystem =='ARIES')%>%
  # filter(is.na(Di.g_SS)==TRUE)%>%
  filter(Shrink>.1 & Shrink <0.9)%>%
  arrange(CustName)

# How many months of each series to plot? (Note each has different start point)
N_MONTHS<-40

for (well in 1:nrow(df_to_plot)) {
  # Get CustName & PropertyId for this row
  CustName.this<-df_to_plot[well,'CustName']%>%pull()
  CustNum.this<-df_to_plot[well,'CustNum']%>%pull()
  propnum.this<-df_to_plot[well,'propnum']%>%pull()
  LeaseName.this<-df_to_plot[well,'LeaseName']%>%pull()
  # Filter data to just that batch
  monthly.this<-monthly.all%>%
    filter(CustomerNumber==CustNum.this)%>%
    filter(PropertyId==propnum.this)
  df.this<-df.all%>%
    filter(CustName==CustName.this)%>%
    filter(propnum==propnum.this)
  
  # Determine if Oil or Gas Decline scraped from Source System
  product_type.this<-if_else(is.na(df.this$qi.o_SS)==FALSE,'Oil','Gas')
  
  # Grab decline parameters corresponding to product_type
  ## Oil Parameters
  if (product_type.this=='Oil')
  {
    # Get CE (Capital Efficiency) Results Parameters
    DateStartHyp_CE.this<-df.this%>%pull(DateStartHyp_CE)
    DateStart_CE.this<-df.this%>%pull(DateStart_CE)
    qi_this.CE<-df.this$qi.o_CE
    Di_this.CE<-df.this$Di.o_CE
    b_this.CE<-df.this$b.o_CE
    # Get SS (Source System) Results Parameters
    DateStart_SS.this<-df.this%>%pull(DateStart_SS)
    qi_this.SS<-df.this$qi.o_SS
    Di_this.SS<-df.this$Di.o_SS
    b_this.SS<-df.this$b.o_SS
    
    # Make Q (Production Series) from Monthly Data into series
    df_series.MO<-monthly.this%>%
      select(NetProductionOil,ForecastMonth)%>%
      arrange(ForecastMonth)%>%
      head(N_MONTHS)%>%
      rename(q_series.MO=NetProductionOil)

  ## Gas Parameters
  } else if (product_type.this=='Gas') {
    # Get CE (Capital Efficiency) Results Parameters
    DateStartHyp_CE.this<-df.this%>%pull(DateStartHyp_CE)
    DateStart_CE.this<-df.this%>%pull(DateStart_CE)
    qi_this.CE<-df.this$qi.g_CE
    Di_this.CE<-df.this$Di.g_CE
    b_this.CE<-df.this$b.g_CE
    # Get SS (Source System) Results Parameters
    DateStart_SS.this<-df.this%>%pull(DateStart_SS)
    qi_this.SS<-df.this$qi.g_SS
    Di_this.SS<-df.this$Di.g_SS
    b_this.SS<-df.this$b.g_SS
    
    # Make Q (Production Series) from Monthly Data into series
    df_series.MO<-monthly.this%>%
      select(NetProductionGas,ForecastMonth)%>%
      arrange(ForecastMonth)%>%
      head(N_MONTHS)%>%
      rename(q_series.MO=NetProductionGas)
  }
  
  # Make Q (Production Series) from CE Results into series
  q_series.CE<-arps.q(arps.decline(qi_this.CE,Di_this.CE,b_this.CE), seq(0, N_MONTHS))
  # Make Q (Production Series) from SS Results into series
  q_series.SS<-arps.q(arps.decline(qi_this.SS,Di_this.SS,b_this.SS), seq(0, N_MONTHS))
  
  # CE: Make Index Column and convert that to Date Column
  df_series.CE<-tibble::rowid_to_column(as.data.frame(q_series.CE), "index")%>%
    mutate(date=as.Date(DateStart_CE.this) + months(index)-days(15))%>%
    select(-index)
  # SS: Make Index Column and convert that to Date Column
  df_series.SS<-tibble::rowid_to_column(as.data.frame(q_series.SS), "index")%>%
    mutate(date=as.Date(DateStart_SS.this) + months(index)-months(1)-days(15))%>%
    select(-index)
  
  # Combine Production Series' into Dataframe with full join on date
  df_series.all<-df_series.CE%>%
    full_join(df_series.SS,by=c("date"="date"))%>%
    full_join(df_series.MO,by=c("date"="ForecastMonth"))
  
  # Get Things to make plot labels
  Interest.this<-head(monthly.this,1)%>%pull(NetRevenueInterest)
  Shrink.this<-monthly.this%>%arrange(ForecastMonth)%>%head(1)%>%pull(ShrinkGas)
  Y_Max<-max(max(q_series.CE),max(q_series.SS))
  Date_MAX<-max(df_series.all$date)
  Date_PLOT<-as.Date(DateStart_SS.this+months(N_MONTHS/2))
  Y_Label<-paste0('Net ',product_type.this)
  reservoir.this<-df_to_plot[well,'reservoir']%>%pull()
  prod_profile.this<-df_to_plot[well,'prod_profile']%>%pull()
  
  # Pivot Wide to Long to make ggplot Happy
  df_ggplot<-df_series.all%>%pivot_longer(c(q_series.CE,q_series.MO,q_series.SS),'series')
  # Make Plot
  print(ggplot(df_ggplot, aes(x=as.Date(date), y=value, color=series)) + 
          geom_point() +
          # ylim(0,10000) +
          ylab(Y_Label)+
          xlab('Date')+
          geom_vline(xintercept=as.Date(DateStart_SS.this)) +
          annotate('text',x=Date_PLOT,y=0,label=
                     paste0('NRI: ',Interest.this,', Shrink: ',Shrink.this)) +
          annotate('text',x=Date_PLOT,y=Y_Max,label=CustName.this) +
          annotate('text',x=Date_PLOT,y=Y_Max*0.9,label=propnum.this) +
          annotate('text',x=Date_PLOT,y=Y_Max*0.8,label=prod_profile.this) +
          annotate('text',x=Date_PLOT,y=Y_Max*0.7,label=reservoir.this) +
          annotate('text',x=Date_PLOT,y=Y_Max*0.6,label=
                   paste0('b diff: ',round(b_this.SS-b_this.CE,2)
                         ,' - Di diff: ',round(Di_this.SS-Di_this.CE,2))) +
        ggtitle(LeaseName.this)
  )
}
